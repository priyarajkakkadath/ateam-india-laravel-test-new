@extends('layouts.app')

@section('content')
<div class="container">
  @if(Session::has('messages'))
  <div class="alert {{ Session::get('messages')['alert-class'] }}" role="alert">
    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
    <span class="sr-only"></span>
    {{ Session::get('messages')['message'] }}
  </div>
  @endif
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('My Profile') }}</div>

                <div class="card-body">
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name"
                                 value="{{ $user->name }}" required autocomplete="name" autofocus>

                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $user->email }}" required autocomplete="email">

                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="gender" class="col-md-4 col-form-label text-md-right">{{ __('Gender') }}</label>

                            <div class="col-md-6">
                                <input value="1" type="radio" class="@error('gender') is-invalid @enderror" name="gender" @if($user->gender == 1) checked @endif required> Male
                                <input value="2" type="radio" class="@error('gender') is-invalid @enderror" name="gender" @if($user->gender == 2) checked @endif required> Female
                                <input value="3" type="radio" class="@error('gender') is-invalid @enderror" name="gender" @if($user->gender == 3) checked @endif required> Others

                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="profile_pic" class="col-md-4 col-form-label text-md-right">{{ __('Profile picture') }}</label>

                            <div class="col-md-6">
                              <img class="rounded-circle" style="width:50px;height:50px;" src="{{route('my.profile.pic.show')}}" />
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                              <a  href="{{ route('profile_edit') }}">
                                <button class="btn btn-primary">
                                    {{ __('Edit') }}
                                </button>
                              </a>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
