<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Friend;
use App\Models\FriendRequest;

class CreateFriendNRequestTables extends Migration
{
    protected $friend_table;
    protected $friend_rqst_table;

    public function __construct() {

      $this->friend_table = (new Friend())->getTable();
      $this->friend_rqst_table = (new FriendRequest())->getTable();

    }

    public function up()
    {
        Schema::create($this->friend_rqst_table, function (Blueprint $table) {
            $table->id();
            $table->BigInteger('request_from_id');
            $table->BigInteger('request_to_id');
            $table->enum('status',[0,1,2])->default(0)->comment('0-rqstd,1-accept,3-reject');
            $table->timestamps();
        });

        Schema::create($this->friend_table, function (Blueprint $table) {
            $table->id();
            $table->BigInteger('user_id');
            $table->BigInteger('friend_id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists($this->friend_rqst_table);
        Schema::dropIfExists($this->friend_table);
    }
}
