@extends('layouts.app')

@section('content')
<div class="container">

  @if(Session::has('messages'))
  <div class="alert {{ Session::get('messages')['alert-class'] }}" role="alert">
    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
    <span class="sr-only"></span>
    {{ Session::get('messages')['message'] }}
  </div>
  @endif

    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">{{ __('My Friends List') }}</div>

                <div class="card-body">
                  @if(count($friends_list) > 0)
                  <table class="table">
                    <thead>
                        <tr>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                         @foreach($friends_list as $val)
                          <tr>
                              <td>
                                <img class="rounded-circle" style="width:50px;height:50px;" src="{{route('profile.pic.show', [$val->id])}}" />
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                {{$val->name}} </td>
                              <td>
                              </td>
                          </tr>
                         @endforeach
                   </tbody>
                  </table>
                  @else
                  <p> No friends found..</p>
                  @endif
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card">
                <div class="card-header">{{ __('Friend Request to you') }}</div>

                <div class="card-body">
                  @if(count($friend_request_list) > 0)
                  <table class="table">
                    <thead>
                        <tr>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                         @foreach($friend_request_list as $val)
                          <tr>
                              <td>
                                <img class="rounded-circle" style="width:50px;height:50px;" src="{{route('profile.pic.show', [$val->id])}}" />
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                 {{$val->name}} </td>
                              <td>

                                <div style="display: inline-flex;">
                                <a class="nav-link" href="{{ route('friend.request.respond', [$val->id,1]) }}">
                                  <button type="button" class="btn btn-success" aria-label="Left Align">
                                      Approve
                                  </button>
                                </a>

                                <a class="nav-link" href="{{ route('friend.request.respond', [$val->id,2]) }}">
                                  <button type="button" class="btn btn-danger" aria-label="Left Align">
                                      Reject
                                  </button>
                                </a>
                              </div>


                              </td>
                          </tr>
                         @endforeach
                   </tbody>
                  </table>
                  @else
                  <p> No friend requests found..</p>
                  @endif
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card">
                <div class="card-header">{{ __('All Users') }}</div>
                <div class="card-body">
                  @if(count($not_friend_list) > 0)
                  <table class="table">
                    <thead>
                        <tr>
                            <th></th>
                            <th> </th>
                        </tr>
                    </thead>
                    <tbody>
                         @foreach($not_friend_list as $val)
                          <tr>
                              <td>
                                <img class="rounded-circle" style="width:50px;height:50px;" src="{{route('profile.pic.show', [$val->id])}}" />
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                 {{$val->name}} </td>
                              <td>
                                @if($val->request_from_id)
                                 &nbsp;&nbsp;&nbsp;
                                  <button type="button" class="btn btn-info" aria-label="Left Align">
                                  Requested
                                  </button>
                                @else
                                <a class="nav-link" href="{{ route('friend.request.send', $val->id) }}">
                                <button type="button" class="btn btn-success" aria-label="Left Align">
                                    Send Request
                                </button>
                              </a>
                                @endif
                              </td>
                          </tr>
                         @endforeach
                   </tbody>
                  </table>
                  @else
                  <p> No users found..</p>
                  @endif
                </div>
            </div>
        </div>

    </div>
</div>
@endsection
