<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Models\Friend;
use App\Models\FriendRequest;
use App\Models\User;
use Storage;
use Response;
use Illuminate\Support\Facades\Validator;


class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
      $not_friend_list = $this->get_not_friend_list();
      $friends_list = $this->get_friends_list();
      $friend_request_list = $this->get_friend_request_list();
      return view('home', compact('not_friend_list', 'friend_request_list', 'friends_list'));
    }

    public function get_friends_list()
    {
      $user_id = Auth::User()->id;
      $friend_table = (new Friend())->getTable();
      $user_table = (new User())->getTable();

      $friend_list = User::select($user_table.'.name',$friend_table.'.id')
                          ->join($friend_table, $friend_table.'.friend_id', $user_table.'.id')
                          ->where('user_id', $user_id)
                          ->orderBy($friend_table.'.id', 'DESC')
                          ->get();
      return $friend_list;
    }

    public function get_not_friend_list()
    {
      $user_id = Auth::User()->id;
      $friend_table = (new Friend())->getTable();
      $friend_rqst_table = (new FriendRequest())->getTable();
      $user_table = (new User())->getTable();

      $friend_id_qry = User::select('friend_id')
                          ->join($friend_table, $user_table.'.id', $friend_table.'.user_id')
                          ->get();

      $friend_ids = [$user_id];

      foreach($friend_id_qry as $val) {
          $friend_ids[] =  $val->friend_id;
      }

      $not_friend_list = User::select($user_table.'.name',$user_table.'.id',$friend_rqst_table.'.request_from_id')
                          ->leftJoin($friend_rqst_table, $friend_rqst_table.'.request_to_id', $user_table.'.id')
                          ->whereNotIn($user_table.'.id', $friend_ids)
                          ->where(function($query) use($friend_rqst_table,$user_id) {
                              $query->where($friend_rqst_table.'.request_from_id', $user_id)
                                    ->orWhereNull($friend_rqst_table.'.request_from_id');
                          })
                          ->where(function($query) use($friend_rqst_table) {
                              $query->where($friend_rqst_table.'.status', '0')
                                    ->orWhereNull($friend_rqst_table.'.status');
                          })
                          ->orderBy($user_table.'.id', 'DESC')
                          ->get();
      return $not_friend_list;
    }

    public function get_friend_request_list()
    {
      $user_id = Auth::User()->id;
      $friend_rqst_table = (new FriendRequest())->getTable();
      $user_table = (new User())->getTable();

      $friend_requests = User::select($user_table.'.name',$friend_rqst_table.'.id')
                          ->join($friend_rqst_table, $friend_rqst_table.'.request_from_id', $user_table.'.id')
                          ->where('status', '0')
                          ->where('request_to_id', $user_id)
                          ->orderBy($friend_rqst_table.'.id', 'ASC')
                          ->get();
      return $friend_requests;
    }

    public function friend_request_send($id)
    {
        $user_id = Auth::User()->id;
        $request_to_id = $id;
        $request_to_exist = User::find($request_to_id);

        if(!$request_to_exist)
        {
          $message_txt = "OOps! No such user exist.";
          $message = ['alert-class' => 'alert-danger', 'message' => $message_txt];
          session()->flash('messages', $message);
          return redirect()->route('home');
        }

        $already_friend_check = Friend::where('user_id', $user_id)->where('friend_id', $request_to_id)->first();

        if($already_friend_check)
        {
          $message_txt = "You and ".$request_to_exist->name." are already friends.";
          $message = ['alert-class' => 'alert-info', 'message' => $message_txt];
          session()->flash('messages', $message);
          return redirect()->route('home');
        }

        $already_send_request_check = FriendRequest::where('request_from_id', $user_id)->where('request_to_id', $request_to_id)->first();

        if($already_send_request_check)
        {
          $message_txt = "You and ".$request_to_exist->name." are already sent a friend request.";
          $message = ['alert-class' => 'alert-info', 'message' => $message_txt];
          session()->flash('messages', $message);
          return redirect()->route('home');
        }

        $new_request = new FriendRequest();
        $new_request->request_from_id = $user_id;
        $new_request->request_to_id = $request_to_id;
        $new_request->save();

        $message_txt = "successfully sent friend request to ".$request_to_exist->name.".";
        $message = ['alert-class' => 'alert-success', 'message' => $message_txt];
        session()->flash('messages', $message);
        return redirect()->route('home');
    }

    public function friend_request_respond($id, $action)
    {
        $user_id = Auth::User()->id;
        $request_exist = FriendRequest::where('id', $id)
                          ->where('status', '0')
                          ->where('request_to_id',$user_id)
                          ->first();

        if(!$request_exist)
        {
          $message_txt = "OOps! No such request exist.";
          $message = ['alert-class' => 'alert-danger', 'message' => $message_txt];
          session()->flash('messages', $message);
          return redirect()->route('home');
        }

        if($action != 1 && $action != 2)
        {
          $message_txt = "OOps! something went wrong.";
          $message = ['alert-class' => 'alert-danger', 'message' => $message_txt];
          session()->flash('messages', $message);
          return redirect()->route('home');
        }

        FriendRequest::where('id',$id)->update(['status' => $action]);

        if($action == 1)
        {
          $new_friend = new Friend();
          $new_friend->user_id = $user_id;
          $new_friend->friend_id = $request_exist->request_from_id;
          $new_friend->save();
          $message_txt = "Request successfully accepted.";
          $message = ['alert-class' => 'alert-success', 'message' => $message_txt];
        }
        else
        {
          $message_txt = "Request successfully rejected.";
          $message = ['alert-class' => 'alert-info', 'message' => $message_txt];
        }

        session()->flash('messages', $message);
        return redirect()->route('home');
    }

    public function profile() {

        $user_id = Auth::User()->id;
        $user = User::find($user_id);
        if(!$user)
        {
          $message_txt = "OOps! something went wrong.";
          $message = ['alert-class' => 'alert-danger', 'message' => $message_txt];
          session()->flash('messages', $message);
          return redirect()->route('home');
        }

        return view('my-profile-view', compact('user'));
    }

    public function profile_edit() {

      $user_id = Auth::User()->id;
      $user = User::find($user_id);

      if(!$user)
      {
        $message_txt = "OOps! something went wrong.";
        $message = ['alert-class' => 'alert-danger', 'message' => $message_txt];
        session()->flash('messages', $message);
        return redirect()->route('home');
      }

      return view('my-profile-edit', compact('user'));
    }

    public function profile_update(Request $request) {

      $user_id = Auth::User()->id;

      return Validator::make($request->all(), [
          'name' => ['required', 'string', 'max:255'],
          'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
          'gender' => ['required', 'integer', 'in:1,2,3'],
          'profile_pic' => ['nullable','mimes:jpeg,png,jpg,','max:2048']
      ]);

      $user = User::where('id', $user_id)->update([
          'name' => $request['name'],
          'email' => $request['email'],
          'gender' => $request['gender'],
      ]);

      if($request['profile_pic'])
      {
          HomeController::update_proile_picture($request, $user_id);
      }

      $message_txt = "Profile details successfully updated.";
      $message = ['alert-class' => 'alert-success', 'message' => $message_txt];
      session()->flash('messages', $message);
      return redirect()->route('profile');
    }

    public function show_profile_file($id)
    {
      $response = $this->show_file($id);
      return $response;
    }

    public function my_show_profile_file()
    {
      $user_id = Auth::User()->id;
      $response = $this->show_file($user_id);
      return $response;
    }

    public function show_file($user_id)
    {
      $file = User::where('id', $user_id)->first();
      $file_path = 'no-profile-picture.jpg';
      if($file)
      {
          if($file->profile_pic_path)
          {
            $file_path = $file->profile_pic_path;
          }
      }

      if(Storage::disk('user_profile_pics')->exists($file_path))
      {
          $contents = Storage::disk('user_profile_pics')->get($file_path);
          $type = Storage::disk('user_profile_pics')->mimeType($file_path);
          $response = Response::make($contents, 200);
          $response->header("Content-Type", $type);
          return $response;
      }
      else
      {
          abort(404);
      }

    }

    public static function update_proile_picture($request, $user_id)
    {
        $profile_pic_name = $user_id.'-'.time().'.'.$request['profile_pic']->getClientOriginalExtension();
        $request['profile_pic']->storeAs('user_profile_pics',$profile_pic_name);
        User::where('id', $user_id)->update(['profile_pic_path' => $profile_pic_name]);
        return 1;
    }

}
