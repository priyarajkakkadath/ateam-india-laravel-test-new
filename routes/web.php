<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => ['web', 'auth']], function () {

  Route::get('/profile', [App\Http\Controllers\HomeController::class, 'profile'])->name('profile');
  Route::get('/my-profile/edit', [App\Http\Controllers\HomeController::class, 'profile_edit'])->name('profile_edit');
  Route::post('/my-profile/edit', [App\Http\Controllers\HomeController::class, 'profile_update'])->name('profile_update');
  Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
  Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
  Route::get('friend/request/send/{id}', [App\Http\Controllers\HomeController::class, 'friend_request_send'])->name('friend.request.send');
  Route::get('friend/request/respond/{id}/action/{action}', [App\Http\Controllers\HomeController::class,
  'friend_request_respond'])->name('friend.request.respond');
  Route::get('/profile-pic/{id}', [App\Http\Controllers\HomeController::class, 'show_profile_file'])->name('profile.pic.show');
  Route::get('my/profile-pic', [App\Http\Controllers\HomeController::class, 'my_show_profile_file'])->name('my.profile.pic.show');


});
