<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\User;

use DB;

use Hash;

class UserSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'test user',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('123456'),
        ]);
    }
}
