<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\User;

class CreateUsersTable extends Migration
{

    protected $user_table;

    public function __construct() {

      $this->user_table = (new User())->getTable();

    }

    public function up()
    {
        Schema::create($this->user_table, function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->enum('gender',[1,2,3])->default(1)->comment('1-male,2-female,3-other');
            $table->string('profile_pic_path')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists($this->user_table);
    }
}
